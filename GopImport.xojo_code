#tag Class
Protected Class GopImport
Inherits ConsoleApplication
	#tag Event
		Function Run(args() as String) As Integer
		  Dim message As String 
		  determineEnvironnement
		  
		  // Extraire la compagnie
		  Dim compagnie As String = "techeol"
		  #If Not DebugBuild Then
		    Dim uboundArgs As Integer = Ubound(args)
		    If uboundArgs = 0 Then
		      message = GenControlModule.PARMCOMPVIDE
		      Goto SendMessage
		    End If
		    If args(1) <> "" Then
		      compagnie = args(1)
		    End If
		  #Endif
		  
		  message = connexionDB(compagnie)
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardEstime
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardRealiseRev
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardRealiseDepCP
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardRealiseDepDeb
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardRealiseDepPTX
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardRealiseMOret
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardrealiseMoContrat
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardrealisepoMCNR
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardrealisepoMRNF
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importDashboardrealisewo
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  //Travaux à planifier
		  //Travaux en cours
		  //Travaux terminés
		  //Travaux complétés
		  //En suspens
		  
		  
		  message = importClient
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importEmploye
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importProjet
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  message = importProjetExtra
		  If message <> "Succes" Then
		    Goto SendMessage
		  End If
		  
		  // Succès
		  SuiteMessage:
		  message = "Importation de la base de donnees " + compagnie + " complete"
		  
		  SendMessage:
		  SendMail(message)
		  // Wait for the mail to finish sending before letting the 
		  // Console app quit.
		  While Not SendMailSocket.Finished And Not SendMailSocket.Error
		    app.DoEvents
		  Wend
		  
		  
		  // Fermeture des base de données
		  bdCMEQ.Close
		  bdCMEQ = Nil
		  bdGOP.Close
		  bdGOP = Nil
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Function connexionDB(compagnie_param As String) As String
		  Dim message As String = "Succes"
		  
		  // Ouverture base de données CMEQ
		  
		  bdCMEQ = New ODBCDatabase
		  bdCMEQ.Host = host
		  bdCMEQ.UserName = GenControlModule.USERCMEQ
		  bdCMEQ.Password = GenControlModule.PASSWORDCMEQ
		  
		  Select Case compagnie_param
		  Case "hlther"
		    bdCMEQ.DataSource = GenControlModule.HLTHERCMEQDATABASENAMEPROD
		    If environnementProp = "Test" Then  // Nous sommes en test
		      bdCMEQ.DataSource = GenControlModule.HLTHERCMEQDATABASENAMETEST
		    ElseIf environnementProp = "Preproduction" Then  // Nous sommes en préproduction
		      bdCMEQ.DataSource = GenControlModule.HLTHERCMEQDATABASENAMEPREPROD
		    End If
		    
		  Case "rpf"
		    bdCMEQ.DataSource = GenControlModule.RPFCMEQDATABASENAMEPROD
		    If environnementProp = "Test" Then  // Nous sommes en test
		      bdCMEQ.DataSource = GenControlModule.RPFCMEQDATABASENAMETEST
		    ElseIf environnementProp = "Preproduction" Then  // Nous sommes en préproduction
		      bdCMEQ.DataSource = GenControlModule.RPFCMEQDATABASENAMEPREPROD
		    End If
		    
		  Case "techeol"
		    bdCMEQ.DataSource = GenControlModule.TECHEOLCMEQDATABASENAMEPROD
		    If environnementProp = "Test" Then  // Nous sommes en test
		      bdCMEQ.DataSource = GenControlModule.TECHEOLCMEQDATABASENAMETEST
		    ElseIf environnementProp = "Preproduction" Then  // Nous sommes en préproduction
		      bdCMEQ.DataSource = GenControlModule.TECHEOLCMEQDATABASENAMEPREPROD
		    End If
		    
		  End Select 
		  
		  If Not (bdCMEQ.Connect) Then
		    message = GMTextConversion(bdCMEQ.ErrorMessage)
		    Return message
		  End If
		  
		  // Ouverture base de données de la compagnie
		  bdGOP = New PostgreSQLDatabase
		  bdGOP.Host = host
		  bdGOP.UserName = GenControlModule.USERTECHEOL
		  bdGOP.Password = GenControlModule.PASSWORDTECHEOL
		  Select Case compagnie_param
		  Case "hlther"
		    bdGOP.DatabaseName = GenControlModule.HLTHERDATABASENAMEPROD
		    If environnementProp = "Test" Then  // Nous sommes en test
		      //bdGOP.DatabaseName  = Globals.HLTHERDATABASENAMETEST
		    End If
		  Case "rpf"
		    bdGOP.DatabaseName = GenControlModule.RPFDATABASENAMEPROD
		    If environnementProp = "Test" Then  // Nous sommes en test
		      //bdGOP.DatabaseName  = Globals.RPFDATABASENAMETEST
		    End If
		  Case "techeol"
		    bdGOP.DatabaseName = GenControlModule.TECHEOLDATABASENAMEPROD
		    If environnementProp = "Test" Then  // Nous sommes en test
		      //bdGOP.DatabaseName  = Globals.TECHEOLDATABASENAMETEST
		    End If
		    
		  End Select 
		  
		  If Not (bdGOP.Connect) Then
		    message = GMTextConversion(bdGOP.ErrorMessage)
		  Else
		    bdGOP.SQLExecute("SET search_path TO dashboard, equip, dbglobal, portail, locking, audit, public;")
		    If  bdGOP .Error Then
		      message = GMTextConversion(bdGOP.ErrorMessage)
		    End If
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub determineEnvironnement()
		  #if DebugBuild then
		    UserSettings = GetFolderItem( "").Parent.Child("ProdOuTest.ini")
		  #else
		    UserSettings = GetFolderItem("ProdOuTest.ini")
		  #Endif
		  
		  Dim folderItem As FolderItem = UserSettings
		  OpenINI(UserSettings)
		  
		  environnementProp = INI_File.Get("ProductionOuTest","Env","")
		  
		  host = HOSTPROD
		  If environnementProp = "Preproduction" Then  // Nous sommes en test
		    host = HOSTPREPRODUCTION
		  ElseIf environnementProp = "Test" Then  // Nous sommes en test
		    host = HOSTTEST
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ecrireTransac(strSQL As String) As String
		  Dim message As String = "Succes"
		  
		  App.bdGOP.SQLExecute("BEGIN TRANSACTION")
		  App.bdGOP.SQLExecute(strSQL)
		  If App.bdGOP.Error Then
		    message = DefineEncoding(App.bdGOP.ErrorMessage, Encodings.UTF8).ToText
		    App.bdGOP.Rollback
		    Return message
		  End If
		  App.bdGOP.Commit
		  
		  Return message
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importClient() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture table adresses pour extraire les clients
		  
		  strSQL = "SELECT * FROM adresses WHERE groupe = 'Client' ORDER BY nom"
		  Dim clientCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  
		  If clientCMEQRS = Nil Then Exit
		  Dim clientTechEolRS As RecordSet
		  
		  While Not  clientCMEQRS.EOF
		    strSQL = "SELECT * FROM client WHERE client_code = '" + clientCMEQRS.Field("ID").StringValue + "'"
		    clientTechEolRS = bdGOP.SQLSelect(strSQL)
		    If clientTechEolRS <> Nil Then
		      // Si le client existe, il faut le détruire et modifier l'information. Sinon, c'est un nouveau client
		      If clientTechEolRS.RecordCount > 0 Then
		        strSQL = "DELETE FROM client WHERE client_id = " + str(clientTechEolRS.Field("client_id").IntegerValue)
		        message = ecrireTransac(strSQL)
		        If message <> "Succes" Then Return message
		        
		        strSQL  = "INSERT INTO client VALUES( " + str(clientTechEolRS.Field("client_id").IntegerValue)
		      Else
		        strSQL  = "INSERT INTO client VALUES(nextval('client_id_seq') "
		      End If
		      strSQL = strSQL +_ 
		      ", '" + ReplaceAll( clientCMEQRS.Field("ID").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("NOM").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("NO_CIVIQUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("RUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("APPARTEMENT").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("VILLE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("PROVINCE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("CODE_POSTAL").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("TELEPHONE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("TELECOPIEUR").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( clientCMEQRS.Field("COURRIEL").StringValue,"'","''") + "'"+ _
		      ", '')"
		      
		      message = ecrireTransac(strSQL)
		      If message <> "Succes" Then Return message
		    End If
		    
		    clientTechEolRS.Close
		    clientCMEQRS.MoveNext
		  Wend
		  
		  clientCMEQRS.Close
		  clientTechEolRS = Nil
		  clientCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardEstime() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture des estimés
		  
		  strSQL = _ 
		  "SELECT Projet, Systeme, Rev_Mo_Mat, identite, AVG(estime_admin) AS Moy_estime_admin, AVG(estime_profit) AS Moy_estime_profit, SUM(estime_heure) AS Somme_estime_heure, " + _
		  "             SUM(estime_montant) AS Somme_estime_montant, SUM(estime_montant_hors_taxe) AS Somme_estime_montant_hors_taxe, SUM(estime_montant_avec_taxe) AS Somme_estime_montant_avec_taxe FROM " + _
		  "    (SELECT takeoff.projet AS Projet, takeoff.identity as identite, takeoff.systeme AS Systeme, 'MO' AS Rev_Mo_Mat, "+ _
		  "            ROUND(AVG(scenarios.admin_mo),2) As estime_admin, " + _
		  "            ROUND(AVG(scenarios.profit_mo),2) As estime_profit, " + _
		  "            SUM(takeoff.heures) AS estime_heure, " + _
		  "            SUM(takeoff.salaires) AS estime_montant, " + _
		  "            SUM(ROUND((takeoff.salaires + (takeoff.salaires * scenarios.admin_mo/100)) / (1 - scenarios.profit_mo/100),2)) As estime_montant_hors_taxe, "+ _
		  "            SUM(ROUND((takeoff.salaires + (takeoff.salaires * scenarios.admin_mo/100)) / (1 - scenarios.profit_mo/100) * (1 + scenarios.pct_tps_new/100 + scenarios.pct_tvp_new/100),2)) As estime_montant_avec_taxe " + _
		  "         FROM takeoff " + _
		  "         INNER JOIN scenarios ON takeoff.Projet = scenarios.projet " + _
		  "                                           AND takeoff.scenario = scenarios.scenario  " + _
		  "         WHERE (takeoff.heures <> 0 OR takeoff.salaires <> 0)  AND scenarios.analyse = '1' " + _
		  "         GROUP BY Projet, identite, Systeme, Rev_Mo_Mat  " + _
		  "    UNION " + _
		  "    SELECT takeoff.projet AS projet, takeoff.identity as identite, takeoff.systeme AS Systeme, 'MAT' AS Rev_Mo_Mat,  "+ _
		  "            ROUND(AVG(scenarios.admin_mat),2) As estime_admin, " + _
		  "            ROUND(AVG(scenarios.profit_mat),2) As estime_profit, " + _
		  "            SUM(takeoff.heures) AS estime_heure, " + _
		  "            SUM(takeoff.materiel) AS estime_montant, " + _
		  "            SUM(ROUND((takeoff.materiel + (takeoff.materiel * scenarios.admin_mat/100)) / (1 - scenarios.profit_mat/100),2)) As estime_montant_hors_taxe, " + _
		  "            SUM(ROUND((takeoff.materiel + (takeoff.materiel * scenarios.admin_mat/100)) / (1 - scenarios.profit_mat/100) * (1 + scenarios.pct_tps_new/100 + scenarios.pct_tvp_new/100),2)) As estime_montant_avec_taxe " + _
		  "        FROM takeoff  " + _
		  "         INNER JOIN scenarios ON takeoff.Projet = scenarios.projet " + _
		  "                                         AND takeoff.scenario = scenarios.scenario  " + _
		  "        WHERE takeoff.materiel <> 0  AND scenarios.analyse = '1' " + _
		  "        GROUP BY Projet, identite, Systeme, Rev_Mo_Mat " + _
		  "    UNION " + _
		  "    SELECT projet AS Projet, identity as identite, '0030' AS Systeme,  'EXMO' AS Rev_Mo_Mat,  " + _
		  "            0 As estime_admin,  0 As estime_profit,  " + _
		  "            heures AS estime_heure, " + _
		  "            salaires AS estime_montant,  " + _
		  "            salaires As estime_montant_hors_taxe, " + _
		  "            0.00 As estime_montant_avec_taxe " + _
		  "     FROM extras  " + _
		  "     WHERE type = '20' AND (heures <> 0 or salaires <> 0) " + _
		  "    UNION " + _
		  "    SELECT projet AS Projet, identity as identite, '0120' AS Systeme, 'EXMAT' AS Rev_Mo_Mat,  " + _
		  "            0 As estime_admin,  0 As estime_profit,  " + _
		  "            0 AS estime_heure, " + _
		  "            depenses AS estime_montant,  " + _
		  "            depenses As estime_montant_hors_taxe, " + _
		  "            0.00 As estime_montant_avec_taxe " + _
		  "     FROM extras  " + _
		  "     WHERE type = '20' AND depenses <> 0 " + _
		  "    UNION " + _
		  "    SELECT projet AS Projet, identity as identite, '0090' AS Systeme,'EXREV' AS Rev_Mo_Mat,  " + _
		  "            0 As estime_admin,  0 As estime_profit,  " + _
		  "            0 AS estime_heure, " + _
		  "            revenus AS estime_montant,  " + _
		  "            revenus As estime_montant_hors_taxe, " + _
		  "            0.00 As estime_montant_avec_taxe " + _
		  "     FROM extras  " + _
		  "     WHERE type = '20' AND revenus <> 0 " + _
		  "  ) " + _
		  "  AS EstimeTable " + _
		  "  GROUP BY Projet, Systeme, Rev_Mo_Mat, identite  " + _
		  "  ORDER BY Projet, Systeme, Rev_Mo_Mat, identite  "
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  strSQL = "DELETE FROM estime "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  If dashboardCMEQRS = Nil Then Exit
		  While Not  dashboardCMEQRS.EOF
		    If dashboardCMEQRS.Field("Somme_estime_montant_avec_taxe").Value = Nil Then
		      strSQL ="INSERT INTO estime (estime_projet_no, estime_systeme, estime_rev_mo_mat, estime_identity, estime_admin, estime_profit, estime_heure, estime_montant, estime_montant_hors_taxe, estime_montant_avec_taxe)  VALUES(" + _ 
		      "'" + ReplaceAll(dashboardCMEQRS.Field("Projet").StringValue,"'","''")  + "'," + _
		      "'" + dashboardCMEQRS.Field("Systeme").StringValue + "'," + _
		      "'" + dashboardCMEQRS.Field("Rev_Mo_Mat").StringValue + "'," + _
		      "" + dashboardCMEQRS.Field("identite").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Moy_estime_admin").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Moy_estime_profit").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Somme_estime_heure").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Somme_estime_montant").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Somme_estime_montant_hors_taxe").StringValue + "," + _
		      "0)"
		    Else
		      strSQL ="INSERT INTO estime (estime_projet_no, estime_systeme, estime_rev_mo_mat, estime_identity, estime_admin, estime_profit, estime_heure, estime_montant, estime_montant_hors_taxe, estime_montant_avec_taxe)  VALUES(" + _ 
		      "'" + ReplaceAll(dashboardCMEQRS.Field("Projet").StringValue,"'","''")  + "'," + _
		      "'" + dashboardCMEQRS.Field("Systeme").StringValue + "'," + _
		      "'" + dashboardCMEQRS.Field("Rev_Mo_Mat").StringValue + "'," + _
		      "" + dashboardCMEQRS.Field("identite").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Moy_estime_admin").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Moy_estime_profit").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Somme_estime_heure").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Somme_estime_montant").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Somme_estime_montant_hors_taxe").StringValue + "," + _
		      "" + dashboardCMEQRS.Field("Somme_estime_montant_avec_taxe").StringValue + ")"
		      
		    End If
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardRealiseDepCP() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les dépenses (Compte à payer)
		  strSQL = _
		  "SELECT Projet, Date_Jour,  (CASE WHEN Commande Is Null Then '' Else Commande END) As Commande, (CASE WHEN Facture Is Null Then '' Else Facture END) As Facture, " + _
		  "            Emp_Entr, GL, Description, SUBSTR(CP,1,2) as SubstrSyst, SUM(revenu) AS SumRevenu FROM " + _
		  "   (SELECT no_projet AS Projet, SUBSTR(date_ft,1,10) AS Date_Jour, 'CP1' AS CP, no_commande_client AS Commande, no_facture AS Facture, c_entreprise AS Emp_Entr, " + _
		  "        GL_revenu_1 AS GL, charte.description AS Description, SUM(revenu_1) AS Revenu" + _ 
		  "        FROM cp_entete " + _ 
		  "        INNER JOIN charte ON cp_entete.gl_revenu_1 = charte.compte " + _
		  "        WHERE revenu_1 <> 0 " + _
		  "        GROUP BY   Projet, Date_Jour, Commande, Facture, Emp_Entr, GL, Description, CP " + _
		  "       UNION " + _
		  "       SELECT no_projet AS Projet, SUBSTR(date_ft,1,10) AS Date_Jour, 'CP2' AS CP, no_commande_client AS Commande, no_facture AS Facture, c_entreprise AS Emp_Entr, " + _
		  "        GL_revenu_2 AS GL, charte.description AS Description, SUM(revenu_2) AS Revenu" + _ 
		  "        FROM cp_entete " + _ 
		  "        INNER JOIN charte ON cp_entete.gl_revenu_2 = charte.compte " + _
		  "        WHERE revenu_2 <> 0 " + _
		  "        GROUP BY   Projet, Date_Jour, Commande, Facture, Emp_Entr, GL, Description, CP " + _
		  "       UNION " + _
		  "       SELECT no_projet AS Projet, SUBSTR(date_ft,1,10) AS Date_Jour, 'CP3' AS CP, no_commande_client AS Commande, no_facture AS Facture, c_entreprise AS Emp_Entr, " + _
		  "        GL_revenu_3 AS GL, charte.description AS Description, SUM(revenu_3) AS Revenu" + _ 
		  "        FROM cp_entete " + _ 
		  "        INNER JOIN charte ON cp_entete.gl_revenu_3 = charte.compte " + _
		  "        WHERE revenu_3 <> 0 " + _
		  "        GROUP BY   Projet, Date_Jour, Commande, Facture, Emp_Entr, GL, Description, CP " + _
		  "       UNION " + _
		  "       SELECT no_projet AS Projet, SUBSTR(date_ft,1,10) AS Date_Jour, 'CP4' AS CP, no_commande_client AS Commande, no_facture AS Facture, c_entreprise AS Emp_Entr, " + _
		  "        GL_revenu_4 AS GL, charte.description AS Description, SUM(revenu_4) AS Revenu" + _ 
		  "        FROM cp_entete " + _ 
		  "        INNER JOIN charte ON cp_entete.gl_revenu_4 = charte.compte " + _
		  "        WHERE revenu_4 <> 0 " + _
		  "        GROUP BY   Projet, Date_Jour, Commande, Facture, Emp_Entr, GL, Description, CP) " + _
		  "   As CPTable " + _
		  "   GROUP BY Projet, Date_Jour, Commande, Facture, Emp_Entr, GL, Description, SubstrSyst " + _
		  "   ORDER BY Projet, Date_Jour, Commande, Facture, Emp_Entr, GL, Description, SubstrSyst "
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  strSQL = "DELETE FROM realisedep "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  While Not  dashboardCMEQRS.EOF
		    // Ajouter le lien avec le destinataire des comptes à payer
		    Dim facture As String = ""
		    If dashboardCMEQRS.Field("Facture").Value <> Nil Then
		      facture = dashboardCMEQRS.Field("Facture").StringValue
		    End If
		    strSQL ="INSERT INTO realisedep (realisedep_projet_no, realisedep_date, realisedep_gl, realisedep_gl_desc, realisedep_gl_emp_entr, realisedep_systeme, realisedep_montant,  " + _
		    "                            realisedep_nas, realisedep_commande, realisedep_facture)  VALUES(" + _ 
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Projet").StringValue,"'","''")  + "'," + _
		    "'" + Left(dashboardCMEQRS.Field("Date_Jour").StringValue,10) + "'," + _
		    "'" + dashboardCMEQRS.Field("GL").StringValue + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Description").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Emp_Entr").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("SubstrSyst").StringValue,"'","''") + "'," + _
		    "" + dashboardCMEQRS.Field("SumRevenu").StringValue + "," + _
		    "''," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Commande").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(facture,"'","''") + "'" + _
		    ")"
		    
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardRealiseDepDeb() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les dépenses (déboursés)
		  strSQL = "SELECT (CASE WHEN detail_pmt.contrat Is Null Then '' Else detail_pmt.contrat END) AS Contrat, " + _ 
		  "        pmt.date_pmt AS Date_pmt, detail_pmt.gl AS Gl, charte.description AS Description, pmt.c_entreprise AS Emp_Entr, " + _
		  "        SUM(detail_pmt.debit) AS Debit, " + _ 
		  "        SUM(detail_pmt.credit) AS Credit, " + _ 
		  "        SUM(detail_pmt.debit-detail_pmt.credit)  AS Depense" + _
		  "        FROM detail_pmt " + _ 
		  "        INNER JOIN pmt     ON pmt.identity  = detail_pmt.identity_pmt" + _
		  "        INNER JOIN charte ON detail_pmt.gl = charte.compte " + _
		  "        GROUP BY   Contrat, Date_pmt, Gl, Description, Emp_Entr " + _
		  "        ORDER BY    Emp_Entr, Contrat, Date_pmt, Gl, Description "
		  //"       AND detail_pmt.contrat = '1516-1012' " + _
		  //"        WHERE detail_pmt.contrat IS NOT NULL AND pmt.date_compense IS NOT NULL " + _
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  
		  // La destruction des enreg a été faite auparavant
		  //strSQL = "DELETE FROM realisedep "
		  //dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  Dim nom_sav As String = ""
		  Dim nas As String = ""
		  While Not  dashboardCMEQRS.EOF
		    If dashboardCMEQRS.Field("Emp_Entr").StringValue <> nom_sav Then
		      // Extraire le NAS
		      strSQL = "SELECT " + _
		      "        (SELECT FIRST py_entete.nas             FROM py_entete WHERE adresses.nom = py_entete.e_prenom || ' ' || py_entete.e_nom )  as nas_emp, " + _
		      "        (SELECT FIRST py_entete.e_nom        FROM py_entete WHERE adresses.nom = py_entete.e_prenom || ' ' || py_entete.e_nom )  as nom_emp, " + _
		      "        (SELECT FIRST py_entete.e_prenom   FROM py_entete WHERE adresses.nom = py_entete.e_prenom || ' ' || py_entete.e_nom )   as prenom_emp,  * " + _
		      "        FROM adresses " + _
		      "       WHERE groupe = 'Employe' " + _
		      "           AND nas_emp IS NOT NULL " + _
		      "           AND  TRIM(adresses.nom) = TRIM('" + dashboardCMEQRS.Field("Emp_Entr").StringValue + "')"
		      
		      Dim nasCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		      nas = ""
		      If nasCMEQRS <> Nil Then
		        nas = nasCMEQRS.Field("nas_emp").StringValue
		        nasCMEQRS.Close
		        nasCMEQRS = Nil
		      End If
		      nom_sav = dashboardCMEQRS.Field("Emp_Entr").StringValue
		      
		    End If
		    
		    strSQL ="INSERT INTO realisedep (realisedep_projet_no, realisedep_date, realisedep_gl, realisedep_gl_desc, realisedep_gl_emp_entr, realisedep_systeme, realisedep_montant,  " + _
		    "                            realisedep_nas, realisedep_commande, realisedep_facture)  VALUES(" + _ 
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Contrat").StringValue,"'","''")  + "'," + _
		    "'" + dashboardCMEQRS.Field("Date_pmt").StringValue + "'," + _
		    "'" + dashboardCMEQRS.Field("Gl").StringValue + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Description").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Emp_Entr").StringValue,"'","''") + "'," + _
		    "'Deb'," + _
		    "" + dashboardCMEQRS.Field("Depense").StringValue + "," + _
		    "'" + nas + "'," + _
		    "''," + _
		    "''" + _
		    ")"
		    
		    //If dashboardCMEQRS.Field("Depense").DoubleValue = 751.41 Then
		    //Dim aaa As String = ""
		    //End If
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardRealiseDepPTX() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les ajustements 
		  strSQL = "SELECT  projet AS Projet, " + _ 
		  "        date_tx AS Date_tx, gl AS Gl, charte.description AS Description, Description AS Emp_Entr, " + _
		  "        SUM(budget) AS Depense" + _
		  "        FROM projets_tx " + _ 
		  "        INNER JOIN charte ON gl = charte.compte " + _
		  "        WHERE type = 'A' AND gl IS NOT NULL " + _
		  "        GROUP BY   Projet, Date_tx, Gl, Description, Emp_Entr " + _
		  "        ORDER BY    Projet, Date_tx, Gl, Description "
		  //"       AND detail_pmt.contrat = '1516-1012' " + _
		  //"        WHERE detail_pmt.contrat IS NOT NULL AND pmt.date_compense IS NOT NULL " + _
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  
		  // La destruction des enreg a été faite auparavant
		  //strSQL = "DELETE FROM realisedep "
		  //dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  While Not  dashboardCMEQRS.EOF
		    
		    strSQL ="INSERT INTO realisedep(realisedep_projet_no, realisedep_date, realisedep_gl, realisedep_gl_desc, realisedep_gl_emp_entr, realisedep_systeme, realisedep_montant,  " + _
		    "                            realisedep_nas, realisedep_commande, realisedep_facture)  VALUES(" + _ 
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Projet").StringValue,"'","''")  + "'," + _
		    "'" + dashboardCMEQRS.Field("Date_tx").StringValue + "'," + _
		    "'" + dashboardCMEQRS.Field("Gl").StringValue + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Description").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Emp_Entr").StringValue,"'","''") + "'," + _
		    "'Ajt'," + _
		    "" + dashboardCMEQRS.Field("Depense").StringValue + "," + _
		    "''," + _
		    "''," + _
		    "''" + _
		    ")"
		    
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardrealiseMoContrat() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour la main d'oeuvre
		  strSQL = "SELECT no_contrat, py_entete.nas, py_entete.e_nom, py_entete.e_prenom, py_entete.date_periode, py_entete.fonction, py_data.date_jour, py_data.metier, py_data.identity_py,  " + _ 
		  "        MAX(hreg) As sumhreg, MAX(hdem) as sumhdem, MAX(hdou) as sumhdou, " + _
		  "        MAX(sreg+sdem+sdou+primeamontant+primehmontant+vacmontant) AS salContPer,  " + _
		  "        MAX(indhMontant-indhTPS-indhTVQ) AS indhMont, " + _
		  "        MAX(aip1Montant) AS aip1Mont , " + _
		  "        0 As salPeriode, " + _
		  "        0 As Applicable, " + _
		  "        0 As PartEmployeur, " + _
		  "        SUM(py_retenues_v.employeur) As CCQ, " + _
		  "        0 As Das, " + _
		  "        MAX(py_data.cssttaux) As Cssttaux, " + _
		  "        0 As Cnesst" + _
		  "        FROM py_data " + _ 
		  "        INNER JOIN py_entete         ON py_data.identity      = py_entete.identity " + _
		  "        LEFT  JOIN py_retenues_v    ON py_data.identity_py = py_retenues_V.identity_py " + _
		  "       GROUP BY no_contrat, py_entete.nas, py_entete.e_nom, py_entete.e_prenom, py_entete.date_periode, py_entete.fonction, py_data.date_jour, py_data.metier, py_data.identity_py " + _
		  "       ORDER BY no_contrat, py_entete.nas, py_entete.date_periode, py_data.date_jour, py_data.identity_py"
		  //"       WHERE py_data.no_contrat = '1516-1012' " + _
		  //"           AND py_entete.nas = '303287882' " + _
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  //Dim nbenreg As Integer = dashboardCMEQRS.RecordCount
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  strSQL = "DELETE FROM realisemocont "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  Dim salPeriode, salContPer, applicable, partEmployeur, cnesst, rbq As String
		  Dim salPeriodeCalcul, salContPerCalcul, applicableCalcul, avImposFedCalcul, partEmployeurCalcul, cnesstCalcul  As Double
		  Dim das, ccq As String
		  Dim date_periode_sav As String = ""
		  Dim nas_sav As String = ""
		  While Not  dashboardCMEQRS.EOF
		    If dashboardCMEQRS.Field("nas").StringValue <> nas_sav Or dashboardCMEQRS.Field("date_periode").StringValue <> date_periode_sav Then
		      // Extraire la valeur du salaire total pour la période
		      strSQL = "SELECT SUM(sreg+sdem+sdou+primeamontant+primehmontant+vacmontant) AS salPeriode" + _
		      "         FROM py_data " + _
		      "         INNER JOIN py_entete ON py_entete.identity = py_data.identity " + _
		      "        WHERE py_entete.nas                = '" + dashboardCMEQRS.Field("nas").StringValue + "'" + _
		      "            AND py_entete.date_periode = '" + dashboardCMEQRS.Field("date_periode").StringValue + "'"
		      
		      Dim salPeriodeCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		      salPeriode = "0"
		      das = "0"
		      If salPeriodeCMEQRS <> Nil Then
		        salPeriode = salPeriodeCMEQRS.Field("salPeriode").StringValue
		        salPeriodeCalcul = salPeriodeCMEQRS.Field("salPeriode").DoubleValue
		        salPeriodeCMEQRS.Close
		        salPeriodeCMEQRS = Nil
		      End If
		      
		      // Extraire les autres valeurs pour faire des calculs
		      strSQL = "SELECT * "    + _ 
		      "         FROM  realisemoret " + _
		      "        WHERE realisemoret_nas                = '" + dashboardCMEQRS.Field("nas").StringValue + "'" + _
		      "            AND realisemoret_date_periode = '" + dashboardCMEQRS.Field("date_periode").StringValue + "'"
		      salContPer = "0"
		      applicable = "0"
		      partEmployeur = "0"
		      cnesst = "0"
		      applicableCalcul = 0
		      partEmployeurCalcul = 0
		      avImposFedCalcul = 0
		      Dim valPeriodeRS As RecordSet = bdGOP.SQLSelect(strSQL)
		      If valPeriodeRS <> Nil Then
		        applicableCalcul = valPeriodeRS.Field("realisemoret_applicable").DoubleValue
		        partEmployeurCalcul = valPeriodeRS.Field("realisemoret_partemployeur").DoubleValue
		        avImposFedCalcul = valPeriodeRS.Field("realisemoret_avimposfed").DoubleValue
		        valPeriodeRS.Close
		        valPeriodeRS = Nil
		      End If
		      
		      nas_sav = dashboardCMEQRS.Field("nas").StringValue
		      date_periode_sav = dashboardCMEQRS.Field("date_periode").StringValue
		    End If
		    
		    If salPeriodeCalcul <> applicableCalcul Then  // Pour debug 
		      Dim nasTrav As String = dashboardCMEQRS.Field("nas").StringValue
		      Dim periodeTrav As String = dashboardCMEQRS.Field("date_periode").StringValue
		      Dim aaa As String = ""
		    End If
		    
		    ccq = "0"
		    If dashboardCMEQRS.Field("CCQ").Value <> Nil Then ccq = dashboardCMEQRS.Field("CCQ").StringValue
		    If salPeriodeCalcul = 0 Then salPeriodeCalcul = 1
		    salContPerCalcul = dashboardCMEQRS.Field("salContPer").DoubleValue
		    das =  Format(salContPerCalcul / salPeriodeCalcul * partEmployeurCalcul,"-######.##")
		    das = Replace(das,",",".")
		    Dim salBrutCont As String = Format(dashboardCMEQRS.Field("salContPer").DoubleValue,"-######.##")
		    salBrutCont = Replace(salBrutCont,",",".")
		    Dim indhCont As String = Format(dashboardCMEQRS.Field("indhMont").DoubleValue,"-######.##")
		    indhCont = Replace(indhCont,",",".")
		    Dim aip1Cont As String = Format(dashboardCMEQRS.Field("aip1Mont").DoubleValue,"-######.##")
		    aip1Cont = Replace(aip1Cont,",",".")
		    Dim csstTauxDouble As Double = dashboardCMEQRS.Field("Cssttaux").DoubleValue
		    cnesstCalcul = (salContPerCalcul/salPeriodeCalcul) * (ApplicableCalcul*(csstTauxDouble/100.0))
		    cnesst = Format(cnesstCalcul,"-######.##")
		    cnesst = Replace(cnesst,",",".")
		    rbq = "0"
		    If dashboardCMEQRS.Field("aip1Mont").Value <> Nil Then
		      Dim metier As String = dashboardCMEQRS.Field("metier").StringValue
		      Dim fonction As String = dashboardCMEQRS.Field("fonction").StringValue
		      //And (Uppercase(Left(fonction,11)) = "ÉLECTRICIEN" Or Uppercase(Left(fonction,8)) = "APPRENTI" Or Uppercase(Left(fonction,16)) = "MONTEUR DE LIGNE") Then
		      If IsNumeric(metier) And dashboardCMEQRS.Field("aip1Mont").DoubleValue <> 0 _
		        And (Uppercase(Left(fonction,11)) = "ÉLECTRICIEN" Or Uppercase(Left(fonction,8)) = "APPRENTI") Then
		        Dim rbqDouble As Double = salContPerCalcul + dashboardCMEQRS.Field("aip1Mont").DoubleValue
		        rbqDouble = rbqDouble * 0.025
		        rbq = Format(rbqDouble,"-######.##")
		        rbq = Replace(rbq,",",".")
		      End If
		    End If
		    
		    strSQL = "INSERT INTO realisemocont (realisemocont_projet_no, realisemocont_nas, realisemocont_nom, realisemocont_prenom, realisemocont_date_periode,  realisemocont_fonction, realisemocont_date_jour, " + _
		    "                              realisemocont_metier, realisemocont_identity_py, realisemocont_sumhreg, realisemocont_sumhdem, realisemocont_sumhdou, realisemocont_salperiode, realisemocont_salbrut, realisemocont_indhmont, " + _
		    "                             realisemocont_aip1mont, realisemocont_applicable, realisemocont_partemployeur, realisemocont_ccq, realisemocont_das, realisemocont_cnesst_taux, realisemocont_cnesst , realisemocont_rbq)  VALUES(" + _ 
		    "'" + ReplaceAll(dashboardCMEQRS.Field("no_contrat").StringValue,"'","''")  + "'," + _
		    "'" + dashboardCMEQRS.Field("nas").StringValue + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("e_nom").StringValue,"'","''") + "',"+ _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("e_prenom").StringValue,"'","''") + "',"+ _
		    "'" + dashboardCMEQRS.Field("date_periode").StringValue + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("fonction").StringValue,"'","''") + "'," + _
		    "'" + dashboardCMEQRS.Field("date_jour").StringValue + "'," + _
		    "'" + dashboardCMEQRS.Field("metier").StringValue + "'," + _
		    "" + dashboardCMEQRS.Field("identity_py").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("sumhreg").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("sumhdem").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("sumhdou").StringValue + "," + _
		    "" + salPeriode + "," + _
		    "" + salBrutCont + "," + _
		    "" + indhCont + "," + _
		    "" + aip1Cont + "," + _
		    "" + applicable + "," + _
		    "" + partEmployeur + "," + _
		    "" + ccq + "," + _
		    "" + das + "," + _
		    "" + dashboardCMEQRS.Field("Cssttaux").StringValue + "," + _
		    "" + cnesst + "," + _
		    "" + rbq + "" + _
		    ")"
		    
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		  
		  //strSQL = "SELECT no_contrat, py_entete.nas, py_entete.e_nom, py_entete.e_prenom, py_entete.date_periode, py_data.date_jour, py_data.identity_py, MAX(hreg) As sumhreg, MAX(hdem) as sumhdem, MAX(hdou) as sumhdou, " + _
		  //"        MAX(sreg+sdem+sdou+primeamontant+vacmontant) AS salContPer ,  " + _
		  //"        0 As salPeriode, " + _
		  //"        0 As Applicable, " + _
		  //"        0 As PartEmployeur, " + _
		  //"        SUM(py_retenues_v.employeur) As CCQ, " + _
		  //"        0 As Das, " + _
		  //"        MAX(py_data.cssttaux) As Cssttaux, " + _
		  //"        0 As Cnesst" + _
		  //"        FROM py_data " + _ 
		  //"        INNER JOIN py_entete         ON py_data.identity      = py_entete.identity " + _
		  //"        LEFT  JOIN py_retenues_v    ON py_data.identity_py = py_retenues_V.identity_py " + _
		  //"       GROUP BY no_contrat, py_entete.nas, py_entete.e_nom, py_entete.e_prenom, py_entete.date_periode, py_data.date_jour, py_data.identity_py " + _
		  //"       ORDER BY no_contrat, py_entete.nas, py_entete.date_periode, py_data.date_jour, py_data.identity_py"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardRealiseMOret() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour la main d'oeuvre par employe, période de paie - déduction de paie et part employeur
		  strSQL = _
		  "SELECT  nas, date_periode, MAX(applicable) As Applicable, SUM(PartEmploye) As PartEmploye, Sum(PartEmployeur) AS PartEmployeur, SUM(AvImposFed) AS AvImposFed, SUM(AvImposProv) AS AvImposProv  FROM " + _
		  "    (SELECT nas, date_periode, MAX(0) As Applicable, SUM(employe) AS PartEmploye, SUM(employeur) AS PartEmployeur, SUM(0) As AvImposFed, SUM(0) As AvImposProv  " + _
		  "       FROM PY_retenues_p " + _
		  "       INNER JOIN py_entete  ON py_retenues_p.identity = py_entete.identity " + _
		  "      GROUP BY nas, date_periode " + _
		  "    UNION " + _
		  "    SELECT nas, date_periode, MAX(applicable) As Applicable,SUM(0) AS PartEmploye, SUM(0) AS PartEmployeur, SUM(0) As AvImposFed, SUM(0) As AvImposProv " + _
		  "      FROM py_retenues_p " + _
		  "      INNER JOIN py_entete  ON py_retenues_p.identity = py_entete.identity " + _
		  "      WHERE py_retenues_p.type = '[AE]' " + _
		  "      GROUP BY nas, date_periode " + _
		  "    UNION " + _
		  "    SELECT nas, date_periode, MAX(0) As Applicable,SUM(0) AS PartEmploye, SUM(0) AS PartEmployeur, SUM(0) As AvImposFed, SUM(EMPLOYEUR) As AvImposProv " + _
		  "      FROM py_retenues_p " + _
		  "      INNER JOIN py_entete  ON py_retenues_p.identity = py_entete.identity " + _
		  "      WHERE py_retenues_p.type IN('ASSGROUPE','REER') " + _
		  "      GROUP BY nas, date_periode " + _
		  "    UNION " + _
		  "    SELECT nas, date_periode, MAX(0) As Applicable, SUM(0) AS PartEmploye, SUM(0) AS PartEmployeur, SUM(EMPLOYEUR) As AvImposFed, SUM(0) As AvImposProv " + _
		  "       FROM py_retenues_p " + _
		  "       INNER JOIN py_entete  ON py_retenues_p.identity = py_entete.identity " + _
		  "      WHERE py_retenues_p.type IN('REER') " + _
		  "     GROUP BY nas, date_periode) " + _
		  "AS retenuesTable " + _
		  "GROUP BY nas, date_periode " + _
		  "ORDER BY nas, date_periode " 
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  //Dim nbenreg As Integer = dashboardCMEQRS.RecordCount
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  strSQL = "DELETE FROM realisemoret "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  While Not  dashboardCMEQRS.EOF
		    strSQL ="INSERT INTO realisemoret (realisemoret_nas, realisemoret_date_periode, realisemoret_applicable, realisemoret_partemploye, realisemoret_partemployeur, realisemoret_avimposfed, realisemoret_avimposprov)  VALUES(" + _ 
		    "'" + dashboardCMEQRS.Field("nas").StringValue + "'," + _
		    "'" + dashboardCMEQRS.Field("date_periode").StringValue + "'," + _
		    "" + dashboardCMEQRS.Field("Applicable").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("PartEmploye").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("PartEmployeur").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("AvImposFed").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("AvImposProv").StringValue + _
		    ")"
		    
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardrealisePoMCNR() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les PO Matériel commandé mais non reçu 
		  strSQL = _
		  "SELECT * FROM " + _
		  "   (SELECT no_projet, no_po, ROUND(SUM(quantite/conversion*(prix*(1-escompte))),2) As Montant " + _
		  "     FROM po_quantites " + _
		  "     INNER JOIN po_detail   ON po_detail.identity_po  = po_quantites.identity_po AND po_detail.no_ligne = po_quantites.no_ligne " + _
		  "     INNER JOIN po_entete ON po_entete.identity_po = po_detail.identity_po " + _ 
		  "     WHERE etat = '010'  " + _
		  "     GROUP BY  no_projet, no_po ) " + _
		  "AS TablePO " + _
		  "WHERE Montant <> 0 " + _
		  "ORDER BY no_projet, no_po "
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  
		  // Destruction des enreg
		  strSQL = "DELETE FROM realisepoMCNR "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  While Not  dashboardCMEQRS.EOF
		    
		    strSQL = "INSERT INTO realisepoMCNR(realisepoMCNR_projet_no, realisepoMCNR_no_po, realisepoMCNR_montant)  VALUES(" + _ 
		    "'" + ReplaceAll(dashboardCMEQRS.Field("no_projet").StringValue,"'","''")  + "'," + _
		    "'" + dashboardCMEQRS.Field("no_po").StringValue + "'," + _
		    "" + dashboardCMEQRS.Field("Montant").StringValue + "" + _
		    ")"
		    
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardrealisePoMRNF() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les PO Matériel reçu mais non facturé 
		  strSQL = _
		  "SELECT * FROM " + _
		  "   (SELECT no_projet, no_po, etat, ROUND(SUM(quantite/conversion*(prix*(1-escompte))),2) As Montant_PO_Recu " + _
		  "     FROM po_quantites " + _
		  "     INNER JOIN po_detail   ON po_detail.identity_po  = po_quantites.identity_po AND po_detail.no_ligne = po_quantites.no_ligne " + _
		  "     INNER JOIN po_entete ON po_entete.identity_po = po_detail.identity_po " + _ 
		  "     WHERE quantite < 0 " + _
		  "     GROUP BY  no_projet, no_po, etat) " + _
		  "AS TablePO " + _
		  "WHERE Montant_PO_Recu < 0 " + _
		  "ORDER BY no_projet, no_po, etat "
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  Dim dashboardGrpRpfBisRS As RecordSet
		  
		  // Destruction des enreg
		  strSQL = "DELETE FROM realisepoMRNF "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  While Not  dashboardCMEQRS.EOF
		    
		    // Lire l'enregistrement correspondant dans les comptes à payer
		    strSQL = "SELECT SUM(realisedep_montant) AS Montant_CP  " + _
		    "         FROM realisedep "  + _
		    "       WHERE realisedep_commande =  '" + dashboardCMEQRS.Field("no_po").StringValue + "'"
		    dashboardGrpRpfBisRS = bdGOP.SQLSelect(strSQL)
		    Dim montant_po_recu As Double = dashboardCMEQRS.Field("Montant_PO_Recu").DoubleValue * -1
		    Dim montant_cp As Double = 0
		    If dashboardGrpRpfBisRS.Field("Montant_CP").Value <> Nil Then montant_cp = dashboardGrpRpfBisRS.Field("Montant_CP").DoubleValue
		    
		    If montant_po_recu <> montant_cp Then
		      Dim montant_Diff As String =  Format(montant_po_recu-montant_cp,"-############.##")
		      montant_Diff = Replace(montant_Diff,",",".")
		      Dim montant_po_recu_car As String = Format(montant_po_recu,"-############.##")
		      montant_po_recu_car = Replace(montant_po_recu_car,",",".")
		      Dim montant_cp_car As String = Format(montant_cp,"-############.##")
		      montant_cp_car = Replace(montant_cp_car,",",".")
		      
		      strSQL = "INSERT INTO realisepoMRNF(realisepoMRNF_projet_no, realisepoMRNF_no_po, realisepoMRNF_etat, realisepoMRNF_montant_po, realisepoMRNF_montant_cp, realisepoMRNF_montant)  VALUES(" + _ 
		      "'" + ReplaceAll(dashboardCMEQRS.Field("no_projet").StringValue,"'","''")  + "'," + _
		      "'" + dashboardCMEQRS.Field("no_po").StringValue + "'," + _
		      "'" + dashboardCMEQRS.Field("etat").StringValue + "'," + _
		      "" + montant_po_recu_car + "," + _
		      "" + montant_cp_car + "," + _
		      "" + montant_Diff + "" + _
		      ")"
		      
		      message = ecrireTransac(strSQL)
		      If message <> "Succes" Then Return message
		    End If
		    dashboardGrpRpfBisRS.Close
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardGrpRpfBisRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardRealiseRev() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les revenus (Facturation)
		  strSQL = _
		  "SELECT ft_entete.no_projet AS Projet, SUBSTR(ft_entete.date_ft,1,10) AS Date_jour, ft_entete. no_facture AS Facture, ft_entete. no_commande_client AS Commande, ft_entete.c_entreprise AS Entr, " + _
		  "        ft_entete.gl_revenu_1 AS GL, charte.description AS Description, ft_entete.revenu_1-ft_entete.escompte AS Revenu,  " + _ 
		  "        ft_entete.re_paiement AS RefPaiement, CASE WHEN ft_entete.paiement = 0 Then 0 Else ft_entete.paiement-ft_entete.escompte-ft_entete.montant_tps-ft_entete.montant_tvp END AS Paiement, " + _ 
		  "        ft_entete.termes As Termes, ft_entete.dernierpmt As Dernier_paiement, ft_entete.ecrituregl As Ecriture_GL, termes.jour_interet As Jour_Interet " + _
		  "        FROM ft_entete " + _ 
		  "        LEFT JOIN charte   ON ft_entete.gl_revenu_1 = charte.compte " + _
		  "        LEFT JOIN termes  ON ft_entete.termes         = termes.terme   " + _
		  "        WHERE ft_entete.revenu_1 <> 0 " + _
		  "UNION " + _
		  "SELECT ft_entete.no_projet AS Projet, SUBSTR(ft_entete.date_ft,1,10) AS Date_jour, ft_entete. no_facture AS Facture, ft_entete. no_commande_client AS Commande, ft_entete.c_entreprise AS Entr, " + _
		  "        ft_entete.gl_revenu_2 AS GL, charte.description AS Description, ft_entete.revenu_2-ft_entete.escompte AS Revenu, " + _ 
		  "        ft_entete.re_paiement AS RefPaiement, CASE WHEN ft_entete.paiement = 0 Then 0 Else ft_entete.paiement-ft_entete.escompte-ft_entete.montant_tps-ft_entete.montant_tvp END AS Paiement, " + _ 
		  "        ft_entete.termes As Termes, ft_entete.dernierpmt As Dernier_paiement, ft_entete.ecrituregl As Ecriture_GL, termes.jour_interet As Jour_Interet " + _
		  "        FROM ft_entete " + _ 
		  "        LEFT JOIN charte   ON ft_entete.gl_revenu_2 = charte.compte " + _
		  "        LEFT JOIN termes  ON ft_entete.termes         = termes.terme   " + _
		  "        WHERE ft_entete.revenu_2 <> 0 " + _
		  "UNION " + _
		  "SELECT ft_entete.no_projet AS Projet, SUBSTR(ft_entete.date_ft,1,10) AS Date_jour, ft_entete. no_facture AS Facture, ft_entete. no_commande_client AS Commande, ft_entete.c_entreprise AS Entr, " + _
		  "        ft_entete.gl_revenu_3 AS GL, charte.description AS Description, ft_entete.revenu_3-ft_entete.escompte AS Revenu, " + _ 
		  "        ft_entete.re_paiement AS RefPaiement, CASE WHEN ft_entete.paiement = 0 Then 0 Else ft_entete.paiement-ft_entete.escompte-ft_entete.montant_tps-ft_entete.montant_tvp END AS Paiement, " + _ 
		  "        ft_entete.termes As Termes, ft_entete.dernierpmt As Dernier_paiement, ft_entete.ecrituregl As Ecriture_GL, termes.jour_interet As Jour_Interet " + _
		  "        FROM ft_entete " + _ 
		  "        LEFT JOIN charte   ON ft_entete.gl_revenu_3 = charte.compte " + _
		  "        LEFT JOIN termes  ON ft_entete.termes         = termes.terme   " + _
		  "        WHERE ft_entete.revenu_3 <> 0 " + _
		  "UNION " + _
		  "SELECT ft_entete.no_projet AS Projet, SUBSTR(ft_entete.date_ft,1,10) AS Date_jour, ft_entete. no_facture AS Facture, ft_entete. no_commande_client AS Commande, ft_entete.c_entreprise AS Entr, " + _
		  "        ft_entete.gl_revenu_4 AS GL, charte.description AS Description, ft_entete.revenu_4-ft_entete.escompte AS Revenu, " + _ 
		  "        ft_entete.re_paiement AS RefPaiement, CASE WHEN ft_entete.paiement = 0 Then 0 Else ft_entete.paiement-ft_entete.escompte-ft_entete.montant_tps-ft_entete.montant_tvp END AS Paiement, " + _ 
		  "        ft_entete.termes As Termes, ft_entete.dernierpmt As Dernier_paiement, ft_entete.ecrituregl As Ecriture_GL, termes.jour_interet As Jour_Interet " + _
		  "        FROM ft_entete " + _ 
		  "        LEFT JOIN charte   ON ft_entete.gl_revenu_4 = charte.compte " + _
		  "        LEFT JOIN termes  ON ft_entete.termes         = termes.terme   " + _
		  "        WHERE ft_entete.revenu_4 <> 0 " + _
		  "        ORDER BY   Projet, Date_Jour, Commande, Facture, Entr, GL, Description "
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  strSQL = "DELETE FROM realiserev "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  While Not  dashboardCMEQRS.EOF
		    Dim termes As String = ""
		    If dashboardCMEQRS.Field("Termes").Value <> Nil Then termes = dashboardCMEQRS.Field("Termes").StringValue
		    Dim dernier_paiement As String = ""
		    If dashboardCMEQRS.Field("Dernier_paiement").Value <> Nil Then 
		      dernier_paiement = mid(dashboardCMEQRS.Field("Dernier_paiement").StringValue,1,10)
		    Else
		      dernier_paiement = dashboardCMEQRS.Field("Date_jour").StringValue
		    End If
		    Dim ecriture_gl As String = ""
		    If dashboardCMEQRS.Field("Ecriture_GL").Value <> Nil Then ecriture_gl = dashboardCMEQRS.Field("Ecriture_GL").StringValue
		    Dim jour_interet As String = "0"
		    If dashboardCMEQRS.Field("Jour_Interet").Value <> Nil Then jour_interet = dashboardCMEQRS.Field("Jour_Interet").StringValue
		    
		    strSQL ="INSERT INTO realiserev (realiserev_projet_no, realiserev_date, realiserev_gl, realiserev_gl_desc, realiserev_entr, realiserev_systeme, realiserev_montant,  " + _
		    "                            realiserev_paiement, realiserev_ref_paiement, realiserev_commande, realiserev_facture, realiserev_termes, realiserev_dernier_paiement, " + _ 
		    "                            realiserev_ecriture_gl, realiserev_jour_interet)  VALUES(" + _ 
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Projet").StringValue,"'","''")  + "'," + _
		    "'" + Left(dashboardCMEQRS.Field("Date_Jour").StringValue,10) + "'," + _
		    "'" + dashboardCMEQRS.Field("GL").StringValue + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Description").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Entr").StringValue,"'","''") + "'," + _
		    "'Fac'," + _
		    "" + dashboardCMEQRS.Field("Revenu").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("Paiement").StringValue + "," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("RefPaiement").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Commande").StringValue,"'","''") + "'," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Facture").StringValue,"'","''") + "'," + _
		    "'" + termes + "'," + _
		    "'" + dernier_paiement + "'," + _
		    "'" + ecriture_gl + "'," + _
		    "" + jour_interet + "" + _
		    ")"
		    
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importDashboardrealiseWo() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les WO Réquisitions (mouvement d'inventaire)
		  strSQL = _
		  "SELECT *, wo_entete.no_projet AS Projet " + _
		  "     FROM wo_detail " + _
		  "     INNER JOIN wo_entete ON wo_entete.identity_wo  = wo_detail.identity_wo " + _ 
		  "     WHERE wo_entete.etat >= '090'  " + _
		  "     ORDER BY  wo_entete.no_projet , wo_entete.identity_wo, wo_detail.no_ligne "
		  
		  Dim dashboardCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  If dashboardCMEQRS = Nil Then Exit
		  
		  Dim dashboardGrpRpfRS As RecordSet
		  
		  // Destruction des enreg
		  strSQL = "DELETE FROM realisewo "
		  dashboardGrpRpfRS = bdGOP.SQLSelect(strSQL)
		  
		  While Not  dashboardCMEQRS.EOF
		    Dim montant_wo As Double = dashboardCMEQRS.Field("cout").DoubleValue * dashboardCMEQRS.Field("qte_prevue").DoubleValue
		    If montant_wo = 0 Then GoTo Suivant
		    Dim montant_wo_car As String = Format(montant_wo,"-############.##")
		    montant_wo_car = Replace(montant_wo_car,",",".")
		    strSQL = "INSERT INTO realisewo(realisewo_projet_no, realisewo_no_wo, realisewo_no_ligne, realisewo_desc_somm, realisewo_montant)  VALUES(" + _ 
		    "'" + ReplaceAll(dashboardCMEQRS.Field("Projet").StringValue,"'","''")  + "'," + _
		    "" + dashboardCMEQRS.Field("identity_wo").StringValue + "," + _
		    "" + dashboardCMEQRS.Field("no_ligne").StringValue + "," + _
		    "'" + ReplaceAll(dashboardCMEQRS.Field("desc_sommaire").StringValue,"'","''")  + "'," + _
		    "" + montant_wo_car + "" + _
		    ")"
		    
		    message = ecrireTransac(strSQL)
		    If message <> "Succes" Then Return message
		    
		    Suivant:
		    dashboardCMEQRS.MoveNext
		  Wend
		  
		  dashboardCMEQRS.Close
		  dashboardGrpRpfRS = Nil
		  dashboardCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importEmploye() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture table adresses pour extraire les Employes
		  
		  //strSQL = "SELECT * FROM adresses WHERE groupe = 'Employe' ORDER BY nom"
		  strSQL = "SELECT " +_
		  "(SELECT FIRST py_entete.nas             FROM py_entete WHERE adresses.nom = py_entete.e_prenom || ' ' || py_entete.e_nom )  as nas_emp, " + _
		  "(SELECT FIRST py_entete.e_nom       FROM py_entete WHERE adresses.nom = py_entete.e_prenom || ' ' || py_entete.e_nom )  as nom_emp,  " + _
		  "(SELECT FIRST py_entete.e_prenom FROM py_entete WHERE adresses.nom = py_entete.e_prenom || ' ' || py_entete.e_nom )   as prenom_emp,  " + _
		  "* " + _
		  "FROM adresses " + _ 
		  "WHERE groupe = 'Employe' " + _
		  "    AND nas_emp IS NOT NULL"
		  
		  Dim EmployeCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  
		  If EmployeCMEQRS = Nil Then Exit
		  Dim EmployeTechEolRS As RecordSet
		  
		  While Not  EmployeCMEQRS.EOF
		    strSQL = "SELECT * FROM Employe WHERE Employe_nas = '" + EmployeCMEQRS.Field("nas_emp").StringValue + "'"
		    EmployeTechEolRS = bdGOP.SQLSelect(strSQL)
		    If EmployeTechEolRS <> Nil Then
		      Dim actifInactif As String = "A"
		      Dim transit, compte As String = ""
		      Dim tReg, tDem, tDou As Double = 0
		      
		      // Si l'Employe existe, il faut le détruire et modifier l'information. Sinon, c'est un nouvel Employe
		      If EmployeTechEolRS.RecordCount > 0 Then
		        // Conserver la valeur Actif/inactif
		        actifInactif = EmployeTechEolRS.Field("employe_statut").StringValue
		        
		        // Extraire les valeurs #Transit, #Compte, Taux temps simple, Taux Temps et demi, Taux temps double
		        strSQL = "SELECT TOP 1 * FROM py_entete " + _ 
		        "LEFT JOIN py_data ON py_data.identity = py_entete.identity " + _
		        "WHERE NAS = '" + ReplaceAll( EmployeCMEQRS.Field("nas_emp").StringValue,"'","''") + "' " + _
		        "ORDER BY date_periode Desc"
		        Dim EmployeCMEQBISRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		        If EmployeCMEQBISRS <> Nil Then
		          transit = EmployeCMEQBisRS.Field("dd_transit").StringValue
		          compte = EmployeCMEQBisRS.Field("dd_compte").StringValue
		          tReg = EmployeCMEQBisRS.Field("treg").DoubleValue
		          tDem = EmployeCMEQBisRS.Field("tdem").DoubleValue
		          tDou = EmployeCMEQBisRS.Field("tdou").DoubleValue
		          EmployeCMEQBISRS.Close
		          EmployeCMEQBISRS = Nil
		        End If
		        
		        // Effectuer l'écriture
		        strSQL = "DELETE FROM Employe WHERE Employe_id = " + str(EmployeTechEolRS.Field("employe_id").IntegerValue)
		        message = ecrireTransac(strSQL)
		        If message <> "Succes" Then Return message
		        
		        strSQL  = "INSERT INTO Employe VALUES( " + str(EmployeTechEolRS.Field("employe_id").IntegerValue)
		      Else
		        strSQL  = "INSERT INTO Employe VALUES(nextval('employe_id_seq') "
		      End If
		      strSQL = strSQL +_ 
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("nas_emp").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("nom_emp").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("prenom_emp").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("NO_CIVIQUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("RUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("VILLE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("PROVINCE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("CODE_POSTAL").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("TELEPHONE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( EmployeCMEQRS.Field("COURRIEL").StringValue,"'","''") + "'"+ _
		      ", '" + actifInactif + "'" + _
		      ", '" + transit + "'" + _
		      ", '" + compte + "'" + _
		      ", " + str(tReg) +  _
		      ", " + str(tDem) +  _
		      ", " + str(tDou) +  " )"
		      
		      message = ecrireTransac(strSQL)
		      If message <> "Succes" Then Return message
		    End If
		    
		    EmployeTechEolRS.Close
		    EmployeCMEQRS.MoveNext
		  Wend
		  
		  EmployeCMEQRS.Close
		  EmployeTechEolRS = Nil
		  EmployeCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importProjet() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture table adresses pour extraire les projets
		  
		  strSQL = "SELECT * FROM projets ORDER BY PROJET "
		  Dim projetCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  
		  If projetCMEQRS = Nil Then Exit
		  Dim projetTechEolRS As RecordSet
		  
		  While Not  projetCMEQRS.EOF
		    strSQL = "SELECT * FROM projet WHERE projet_no = '" + projetCMEQRS.Field("PROJET").StringValue + "'"
		    projetTechEolRS = bdGOP.SQLSelect(strSQL)
		    If projetTechEolRS <> Nil Then
		      // Si c'est un nouveau projet, on créé un nouvel enregistrement et, plus loin, on modifie l'information.
		      Dim cle_modif As Integer = 0
		      If projetTechEolRS.RecordCount > 0 Then
		        cle_modif = projetTechEolRS.Field("projet_id").IntegerValue
		      Else
		        strSQL ="INSERT INTO projet (projet_id)  VALUES(nextval('projet_id_seq')) "
		        message = ecrireTransac(strSQL)
		        If message <> "Succes" Then Return message
		        // Extraire le numéro de projet ajouté
		        Dim genRS As RecordSet = bdGOP.SQLSelect("SELECT last_value FROM dbglobal.projet_id_seq")
		        cle_modif = genRS.Field("last_value").IntegerValue  // rs.IdxField(1).IntegerValue
		        genRS.Close
		        genRS = Nil
		      End If
		      
		      strSQL = "UPDATE projet SET " +_ 
		      "  projet_no = '" + ReplaceAll( projetCMEQRS.Field("PROJET").StringValue,"'","''") + "'"+ _
		      ", projet_titre = '" + ReplaceAll( projetCMEQRS.Field("TITRE").StringValue,"'","''") + "'"+ _
		      ", projet_type = '" + projetCMEQRS.Field("TYPE").StringValue + "'"+ _
		      ", projet_client_nom =  '" + ReplaceAll( projetCMEQRS.Field("NOM_CLIENT").StringValue,"'","''") + "'"+ _
		      ", projet_estimateur_nom = '" + ReplaceAll( projetCMEQRS.Field("NOM_ESTIMATEUR").StringValue,"'","''") + "'"+ _
		      ", projet_fermeture_date = '" + ReplaceAll( projetCMEQRS.Field("FERMETURE_DATE").StringValue,"'","''") + "'"+ _
		      ", projet_fermeture_lieu = '" + ReplaceAll( projetCMEQRS.Field("FERMETURE_LIEU").StringValue,"'","''") + "'"+ _
		      ", projet_statut = '" + ReplaceAll( projetCMEQRS.Field("DEPOT_NOTE").StringValue,"'","''") + "'"+ _
		      ", projet_charge_projet_nom = '" + ReplaceAll( projetCMEQRS.Field("CAUTION_NOTE").StringValue,"'","''") + "'"+ _
		      ", projet_profit = " + projetCMEQRS.Field("PROFIT").StringValue + ""+ _
		      ", projet_administration = " + projetCMEQRS.Field("ADMINISTRATION").StringValue + ""+ _
		      ", projet_tps = " +  projetCMEQRS.Field("TPS").StringValue + ""+ _
		      ", projet_tvp = " +  projetCMEQRS.Field("TVP").StringValue + ""+ _
		      ", projet_dernier_enregistrement = '" + ReplaceAll( projetCMEQRS.Field("DERNIER_ENREGISTREMENT").StringValue,"'","''") + "'"+ _
		      ", projet_archive_projet = '" + ReplaceAll( projetCMEQRS.Field("ARCHIVE_PROJET").StringValue,"'","''") + "'"+ _
		      ", projet_archive_contrat = '" + ReplaceAll( projetCMEQRS.Field("ARCHIVE_CONTRAT").StringValue,"'","''") + "'" + _
		      " WHERE projet_id = " + str(cle_modif)
		      
		      // Traiter les cas où il y a des numériques vides
		      strSQL = Replace(strSQL, "projet_profit = ,", "projet_profit = 0,")
		      strSQL = Replace(strSQL, "projet_administration = ,", "projet_administration = 0,")
		      strSQL = Replace(strSQL, "projet_tps = ,", "projet_tps = 0,")
		      strSQL = Replace(strSQL, "projet_tvp = ,", "projet_tvp = 0,")
		      
		      message = ecrireTransac(strSQL)
		      If message <> "Succes" Then Return message
		      
		      // Résoudre les liens de certaines zones
		      Dim genRS As RecordSet
		      
		      // Résoudre le lien projet_client
		      genRS = bdGOP.SQLSelect("SELECT client_code  FROM client WHERE client_nom = '" + _
		      ReplaceAll( projetCMEQRS.Field("NOM_CLIENT").StringValue,"'","''")  + "'  LIMIT 1")
		      If genRS.RecordCount > 0 Then
		        strSQL = "UPDATE projet SET " + _
		        "projet_client = '" + genRS.Field("client_code").StringValue + "'" + _
		        " WHERE projet_id = " + str(cle_modif)
		        message = ecrireTransac(strSQL)
		        If message <> "Succes" Then Return message
		      End If
		      genRS.Close
		      
		      // Résoudre le lien projet_estimateur
		      genRS = bdGOP.SQLSelect("SELECT employe_nas  FROM employe WHERE employe_prenom || ' ' || employe_nom = '" + _
		      ReplaceAll( projetCMEQRS.Field("NOM_ESTIMATEUR").StringValue,"'","''")  + "'  LIMIT 1")
		      If genRS.RecordCount > 0 Then
		        strSQL = "UPDATE projet SET " + _
		        "projet_estimateur = '" + genRS.Field("employe_nas").StringValue + "'" + _
		        " WHERE projet_id = " + str(cle_modif)
		        message = ecrireTransac(strSQL)
		        If message <> "Succes" Then Return message
		      End If
		      genRS.Close
		      
		      // Résoudre le lien projet_charge_projet
		      genRS = bdGOP.SQLSelect("SELECT employe_nas  FROM employe WHERE employe_prenom || ' ' || employe_nom = '" + _
		      ReplaceAll( projetCMEQRS.Field("CAUTION_NOTE").StringValue,"'","''")  + "'  LIMIT 1")
		      If genRS.RecordCount > 0 Then
		        strSQL = "UPDATE projet SET " + _
		        "projet_charge_projet = '" + genRS.Field("employe_nas").StringValue + "'" + _
		        " WHERE projet_id = " + str(cle_modif)
		        message = ecrireTransac(strSQL)
		        If message <> "Succes" Then Return message
		      End If
		      genRS.Close
		      
		      genRS = Nil
		    End If
		    
		    projetTechEolRS.Close
		    projetCMEQRS.MoveNext
		  Wend
		  
		  projetCMEQRS.Close
		  projetTechEolRS = Nil
		  projetCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function importProjetExtra() As String
		  Dim message As String = "Succes"
		  Dim strSQL As String = ""
		  
		  // Lecture table adresses pour extraire les projet_extras
		  
		  strSQL = "SELECT * FROM EXTRAS ORDER BY PROJET, SEQUENCE "
		  Dim projet_extraCMEQRS As RecordSet = bdCMEQ.SQLSelect(strSQL)
		  
		  If projet_extraCMEQRS = Nil Then Exit
		  Dim projet_extraTechEolRS As RecordSet
		  
		  While Not  projet_extraCMEQRS.EOF
		    strSQL = "SELECT * FROM projet_extra WHERE projet_extra_projet_no = '" + projet_extraCMEQRS.Field("PROJET").StringValue + "'" + _
		    "                                                               AND projet_extra_sequence =  " + projet_extraCMEQRS.Field("SEQUENCE").StringValue
		    projet_extraTechEolRS = bdGOP.SQLSelect(strSQL)
		    If projet_extraTechEolRS <> Nil Then
		      // Si le projet_extra existe, il faut le détruire et modifier l'information. Sinon, c'est un nouveau projet_extra
		      If projet_extraTechEolRS.RecordCount > 0 Then
		        strSQL = "DELETE FROM projet_extra WHERE projet_extra_id = " + str(projet_extraTechEolRS.Field("projet_extra_id").IntegerValue)
		        message = ecrireTransac(strSQL)
		        If message <> "Succes" Then Return message
		        
		        strSQL  = "INSERT INTO projet_extra VALUES( " + str(projet_extraTechEolRS.Field("projet_extra_id").IntegerValue)
		      Else
		        strSQL  = "INSERT INTO projet_extra VALUES(nextval('projet_extra_id_seq') "
		      End If
		      
		      strSQL = strSQL +_ 
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("PROJET").StringValue,"'","''") + "'"+ _
		      ", " + ReplaceAll( projet_extraCMEQRS.Field("SEQUENCE").StringValue,"'","''") + ""+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("TITRE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("DESCRIPTION").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("NO_FACTURE").StringValue,"'","''") + "'"+ _
		      ", " + ReplaceAll( projet_extraCMEQRS.Field("NO_WKO").StringValue,"'","''") + ""+ _
		      ", " + ReplaceAll( projet_extraCMEQRS.Field("NO_PERMIS").StringValue,"'","''") + ""+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("NO_COMMANDE_CLIENT").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("DATE_TX").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("DATE_TRAVAUX").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("EMETTEUR").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("TYPE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("ETAT").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("MEMO").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_NOM").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_ENTREPRISE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_NOCIVIQUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_RUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_SUITE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_VILLE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_PROVINCE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_ZIP").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_TEL").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_FAX").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("C_COURRIEL").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_NUMERO").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_NOM").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_ENTREPRISE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_NOCIVIQUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_RUE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_SUITE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_VILLE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_PROVINCE").StringValue,"'","''") + "'"+ _
		      ", '" + ReplaceAll( projet_extraCMEQRS.Field("J_ZIP").StringValue,"'","''") + "'"+ _
		      ", " + ReplaceAll( projet_extraCMEQRS.Field("HEURES").StringValue,"'","''") + ""+ _
		      ", " + ReplaceAll( projet_extraCMEQRS.Field("SALAIRES").StringValue,"'","''") + ""+ _
		      ", " + ReplaceAll( projet_extraCMEQRS.Field("REVENUS").StringValue,"'","''") + ""+ _
		      " )"
		      
		      strSQL = Replace(strSQL, ", , ,", ",0,0,")
		      strSQL = Replace(strSQL, ", ,", ",0,")
		      
		      message = ecrireTransac(strSQL)
		      If message <> "Succes" Then Return message
		    End If
		    
		    projet_extraTechEolRS.Close
		    projet_extraCMEQRS.MoveNext
		  Wend
		  
		  projet_extraCMEQRS.Close
		  projet_extraTechEolRS = Nil
		  projet_extraCMEQRS = Nil
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenINI(strFile as FolderItem)
		  INI_File=new INISettings(strFile)
		  INI_File.Load
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub sendMail(messageAEnvoyer As String)
		  SendMailSocket = New SMTP
		  SendMailSocket.Address = "mail.exchange.telus.com" // your SMTP email server
		  SendMailSocket.Port = 25 // Check your server for the property port # to use
		  //SendMailSocket.ConnectionType = SendMailSocket.TLSv1
		  SendMailSocket.Username = "gestop.message@rpf.ca"
		  SendMailSocket.Password = "ML62016!"
		  
		  // Create the actual email message
		  Dim mail As New EmailMessage
		  mail.FromAddress = "no_reply@rpf.ca"
		  mail.Subject = " Importation de base de donnees"
		  mail.BodyPlainText = DefineEncoding(messageAEnvoyer + " " + environnementProp, Encodings.UTF8)
		  mail.Headers.AppendHeader("X-Mailer", "Importation") // Sample header
		  mail.AddRecipient("gestop.message@techeol.com")
		  
		  // Add the message to the SMTPSocket and send it
		  SendMailSocket.Messages.Append(mail)
		  SendMailSocket.SendMail
		  System.DebugLog "done"
		End Sub
	#tag EndMethod


	#tag Note, Name = Version
		Version 07
		
	#tag EndNote


	#tag Property, Flags = &h0
		bdCMEQ As ODBCDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		bdGOP As PostgreSQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		centralDatabaseName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		environnementProp As String
	#tag EndProperty

	#tag Property, Flags = &h0
		host As String
	#tag EndProperty

	#tag Property, Flags = &h0
		INI_File As INISettings
	#tag EndProperty

	#tag Property, Flags = &h0
		SendMailSocket As SMTP
	#tag EndProperty

	#tag Property, Flags = &h0
		UserSettings As FolderItem
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="centralDatabaseName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="environnementProp"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="host"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
