#tag Module
Protected Module GenControlModule
	#tag Method, Flags = &h0
		Function extract_Field_Name(field As String) As String
		  
		  // Extraire le nom du champ en laissant le suffixe avec le souligné
		  
		  Dim fieldTable(-1) As String = Split(field, "_")
		  
		  Dim indexLastElement As Integer = Ubound(fieldTable)
		  
		  Return Replace(field, "_"+fieldTable(IndexLastElement), "")
		  
		End Function
	#tag EndMethod


	#tag Constant, Name = CARRIAGERETURN, Type = String, Dynamic = True, Default = \"\r", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HLTHERCMEQDATABASENAMEPREPROD, Type = String, Dynamic = True, Default = \"Cie02", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HLTHERCMEQDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"Cie02", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HLTHERCMEQDATABASENAMETEST, Type = String, Dynamic = True, Default = \"CMEQ Cie02 32 bits", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HLTHERDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"hlther", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HLTHERDATABASENAMETEST, Type = String, Dynamic = True, Default = \"hlther", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ml6cartier01.cartierenergie.com"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOTEST, Type = String, Dynamic = True, Default = \"192.168.1.86", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPREPRODUCTION, Type = String, Dynamic = True, Default = \"192.168.1.11", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPROD, Type = String, Dynamic = True, Default = \"192.168.1.10", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTTEST, Type = String, Dynamic = True, Default = \"192.168.1.86", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LEAVEAPPMESSAGE, Type = String, Dynamic = True, Default = \"Vous \xC3\xAAtes en train de quitter cette application.\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You are about to leave this application."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
	#tag EndConstant

	#tag Constant, Name = PARMCOMPVIDE, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Company parameter is empty"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Company parameter is empty"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Param\xC3\xA8tre compagnie est vide"
	#tag EndConstant

	#tag Constant, Name = PASSWORDCMEQ, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"sql"
	#tag EndConstant

	#tag Constant, Name = PASSWORDTECHEOL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"postgres"
	#tag EndConstant

	#tag Constant, Name = PHOTOFOLDER, Type = String, Dynamic = True, Default = \"baseplan/reppale", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"baseplan/reppale"
	#tag EndConstant

	#tag Constant, Name = RPFCMEQDATABASENAMEPREPROD, Type = String, Dynamic = True, Default = \"Cie01", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RPFCMEQDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"Cie01", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RPFCMEQDATABASENAMETEST, Type = String, Dynamic = True, Default = \"CMEQ Cie01 32 bits", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RPFDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"rpf", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RPFDATABASENAMETEST, Type = String, Dynamic = True, Default = \"rpf", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TECHEOLCMEQDATABASENAMEPREPROD, Type = String, Dynamic = True, Default = \"Cie05", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TECHEOLCMEQDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"Cie05", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TECHEOLCMEQDATABASENAMETEST, Type = String, Dynamic = True, Default = \"CMEQ Cie05 32 bits", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TECHEOLDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"techeol", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TECHEOLDATABASENAMETEST, Type = String, Dynamic = True, Default = \"techeol", Scope = Public
	#tag EndConstant

	#tag Constant, Name = USERCMEQ, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"dba"
	#tag EndConstant

	#tag Constant, Name = USERTECHEOL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"admin"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
